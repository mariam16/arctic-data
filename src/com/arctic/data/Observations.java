package com.arctic.data;

public class Observations{
	private String station;
	private String valid;
	private String tempf;
	private String relh;
	
	public Observations(String station, String valid, String tempf, String relh) {
		this.station = station;
		this.valid = valid;
		this.tempf = tempf;
		this.relh = relh;
	}
	
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public String getValid() {
		return valid;
	}
	public void setValid(String valid) {
		this.valid = valid;
	}
	public String getTempf() {
		return tempf;
	}
	public void setTempf(String tempf) {
		this.tempf = tempf;
	}
	public String getRelh() {
		return relh;
	}
	public void setRelh(String relh) {
		this.relh = relh;
	}
	
	
}