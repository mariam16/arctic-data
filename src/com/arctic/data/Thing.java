package com.arctic.data;

public class Thing{
	private String station;
	private String description;
	private String properties;
	private double lat;
	private double lon;
	private double elevation;
	private String begInts;
	private String iem_network;
	
	public Thing(String station,String description, String properties, double lat,double lon,double elevation,String begInts,String iem_network){
		this.description=description;
		this.properties=properties;
		this.lat=lat;
		this.lon=lon;
		this.elevation=elevation;
		this.begInts= begInts;
		this.iem_network= iem_network;
		this.station=station;
	}
	
	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getBegInts() {
		return begInts;
	}

	public void setBegInts(String begInts) {
		this.begInts = begInts;
	}

	public String getIem_network() {
		return iem_network;
	}

	public void setIem_network(String iem_network) {
		this.iem_network = iem_network;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public void setDescription(String value){
		this.description= value;
	}
	
	public String getDescription(){
		return this.description;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getElevation() {
		return elevation;
	}

	public void setElevation(double elevation) {
		this.elevation = elevation;
	}
	
	
}