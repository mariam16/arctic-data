package com.arctic.data;

public class MesonetData{
	private String station;
	private String description;
	private String unit;
	
	public MesonetData(String station, String description, String unit) {
		super();
		this.station = station;
		this.description = description;
		this.unit = unit;
	}
	
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
}