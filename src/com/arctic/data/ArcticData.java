package com.arctic.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ArcticData{
	private static List<Thing> thingList;
	private static List<Sensor> sensorList;
	private static List<Observations> observationsList;
	private static HashMap<String, Thing> hashMapThing=new HashMap<String,Thing>();
	private static HashMap<String, Sensor> hashMapSensor=new HashMap<String,Sensor>();
	private static HashMap<String, String> hashmapObservedProperty=new HashMap<String,String>();
	private static HashMap<Integer, MesonetData> hashmapDatastream=new HashMap<Integer,MesonetData>();
	
	/**
	 * Read in all the stations and their corresponding information. Store the information in the thingList and sensorList array.
	 */
	public static void readFromURL(){
		try {
			//open stream (reader) to read contents from the URL
			URL url = new URL("https://mesonet.agron.iastate.edu/sites/networks.php?network=CA_NT_ASOS&format=csv&nohtml=on");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			
			//read contents from the URL
			String inputLine=reader.readLine();
			
			//initialize the lists
			thingList= new ArrayList<Thing>();
			sensorList= new ArrayList<Sensor>();
			
			//read each line and parse the line using the identifier ","
			while((inputLine = reader.readLine()) != null){
				String[] token = inputLine.split(",");
				
				Thing objThing = new Thing(token[0],token[0]+"-"+token[1].trim(),null,Double.parseDouble(token[2]),Double.parseDouble(token[3]),Double.parseDouble(token[4]), token[5], token[6]);
				thingList.add(objThing);
				
				Sensor objSensor = new Sensor(token[0],token[0]+"-"+token[1].trim(),token[5], token[6]);
				sensorList.add(objSensor);
				
				//System.out.println(inputLine+"\n"+ objThing.getDescription());
			}

			reader.close();
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Read all Observations and store it in the observationList array.
	 */
	public static void readFromObsURL(String previousDate, String currentDate){
		
			//open stream (reader) to read contents from the URL
			URL url;
			try {
				String [] pDateToken = previousDate.split("-");
				String [] tDateToken = currentDate.split("/");
				url = new URL("http://mesonet.agron.iastate.edu/cgi-bin/request/asos.py?station=CYKD&data=all&year1="+pDateToken[0]+"&month1="+pDateToken[1]+"&day1="+pDateToken[2].substring(0, 2)+"&year2="+tDateToken[0]+"&month2="+tDateToken[1]+"&day2="+tDateToken[2].substring(0, 2)+"&tz=Etc%2FUTC&format=tdf&latlon=no&direct=no");
			
				BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			
				//read contents from the URL
				String inputLine=reader.readLine();
				for(int i=0; i<5; i++){
					reader.readLine();
				}
				
				observationsList= new ArrayList<Observations>();
				
				//read each line and parse the line using the identifier ","
				while((inputLine = reader.readLine()) != null){
					//System.out.println(inputLine);
					String[] token = inputLine.split("\\s+");
					
					Observations observ= new Observations(token[0],token[1]+" "+token[2],token[3], token[5]);
					observationsList.add(observ);
				}

				reader.close();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/**
	 * Creates the entities for each station Thing, Locations, Sensor, and Datastream.
	 * Each stations has two datastreams (relative humidity and temperature).
	 * @param key
	 * @param unitTemp
	 * @param obserPropertyTemp
	 * @param unitRelh
	 * @param observedPropertyRelh
	 * @param sensor
	 */
	public static void addThingToDatabase(Thing key, JSONObject unitTemp, JSONObject obserPropertyTemp, JSONObject unitRelh, JSONObject observedPropertyRelh, JSONObject sensor){
		try {
			URL url = new URL("http://192.168.1.13:8080/OGCSensorThings/v1.0/Things");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			
			connection.setDoOutput(true);
			OutputStream os= connection.getOutputStream();
			
			JSONObject json = new JSONObject();
			json.put("description", key.getDescription());
			JSONObject propertyObj = new JSONObject();
			propertyObj.put("begints", key.getBegInts());
			propertyObj.put("iem_network", key.getIem_network());
			json.put("properties", propertyObj);
			JSONArray array = new JSONArray();
				
			JSONObject encodeType = new JSONObject();
			encodeType.put("encodingType", "application/vnd.geo+json");
			JSONObject descripLocation = new JSONObject();
			descripLocation.put("description", key.getDescription());
				
			JSONObject type = new JSONObject();
			type.put("type", "Point");
				
			JSONArray coordArray = new JSONArray();
			coordArray.put(key.getLat());
			coordArray.put(key.getLon());
			coordArray.put(key.getElevation());
				
			JSONObject location = new JSONObject();
			location.put("type", "Point");
			location.put("coordinates", coordArray);
				
			JSONObject locationData = new JSONObject();
			locationData.put("encodingType", "application/vnd.geo+json");
			locationData.put("description", key.getDescription());
			locationData.put("location", location);

			array.put(locationData);
			
			JSONObject datastreamObj = new JSONObject();
			datastreamObj.put("description", "This is a datastream for measuring the temperature typically at 2 meters");
			datastreamObj.put("observationType", "");
			datastreamObj.put("unitOfMeasurement", unitTemp);
			datastreamObj.put("ObservedProperty", obserPropertyTemp);
			datastreamObj.put("Sensor", sensor);
			
			JSONObject datastreamObj2 = new JSONObject();
			datastreamObj2.put("description", "This is a datastream for measuring relative humidity (%)");
			datastreamObj2.put("observationType", "");
			datastreamObj2.put("unitOfMeasurement", unitRelh);
			datastreamObj2.put("ObservedProperty", observedPropertyRelh);
			datastreamObj2.put("Sensor", sensor);
			
			JSONArray datastreamArray = new JSONArray();
			datastreamArray.put(datastreamObj);
			datastreamArray.put(datastreamObj2);
			
			json.put("Locations", array);
			json.put("Datastreams", datastreamArray);	
			
			os.write(json.toString().getBytes("UTF-8"));
			os.flush();
			os.close();
			
			//201 created
			int responseCode= connection.getResponseCode();
				
			if(responseCode != 201){
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputLine;
					
				while((inputLine=reader.readLine()) !=null){
					System.out.println(inputLine);
				}
			}else{
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputLine;
					
				while((inputLine=reader.readLine()) !=null){
					String [] token = inputLine.split(",");
					String [] tokenizer = null;
					
					for(int i=0; i< token.length; i++){
						if(token[i].contains("id")){
							tokenizer = token[i].split(":");
							break;
						}
					}
					hashMapThing.put(tokenizer[1], key);
				}
			}
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Get the datastream ID that corresponds to a Thing ID.
	 * @param thingID
	 * @param thing
	 */
	public static void getDatastreamID(String thingID, Thing thing){
		URL url;
		try {
			url = new URL("http://192.168.1.13:8080/OGCSensorThings/v1.0/Things("+thingID+")/Datastreams");
		
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			
			int responseCode = connection.getResponseCode();
			
			if(responseCode != 200){
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputLine;
			
				while((inputLine=reader.readLine()) !=null){
					System.out.println("Error:"+inputLine);
				}
			}else{
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputLine;
				String jsonString = null;
				
				while((inputLine=reader.readLine()) !=null){
					jsonString = inputLine;
				}
				try {
					JSONObject obj = new JSONObject(jsonString.trim());
					JSONArray resultArray = obj.getJSONArray("value");
					
					for(int i=0; i< resultArray.length(); i++){
						JSONObject value =resultArray.getJSONObject(i);
						JSONObject unit = value.getJSONObject("unitOfMeasurement");
						MesonetData data = new MesonetData(thing.getStation(),thing.getDescription(),unit.getString("name"));
						hashmapDatastream.put(value.getInt("id"), data);
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Add the observations to the IoT API.
	 * Since temperature is in farenheight convert it manually to celsius.
	 * @param result
	 * @param time
	 * @param dsID2
	 */
	public static void addObservationsToDB(String result, String time, int dsID2){
		URL url;
		try {
			url = new URL("http://192.168.1.13:8080/OGCSensorThings/v1.0/Observations");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");

			connection.setDoOutput(true);
			OutputStream os= connection.getOutputStream();
			
			JSONObject json = new JSONObject();
			
			//manual conversion of temperature from F to C
			float tempF= Float.parseFloat(result);
			float tempC = (tempF-32)/(9/5);
			json.put("result", Float.toString(tempC));
			
			json.put("phenomenonTime", time);
			JSONObject dsID = new JSONObject();
			dsID.put("id", dsID2);
			json.put("Datastream", dsID);
			
			os.write(json.toString().getBytes("UTF-8"));
			os.flush();
			os.close();
	
			//200 created
			int responseCode = connection.getResponseCode();
			
			if(responseCode != 201){
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputLine;
			
				while((inputLine=reader.readLine()) !=null){
					System.out.println("Error: "+inputLine);
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String [] args){
		Runnable run = new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					readFromURL();
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					Date date = new Date();
					System.out.println("Todays Date: "+dateFormat.format(date));
					
					File file = new File("update.txt");
					
					if(file.exists() && !file.isDirectory()){
						BufferedReader br = new BufferedReader(new FileReader("update.txt"));
						String line;
						String previousDate = null;
						
						while((line=br.readLine()) != null){
							previousDate = line;
						}
						System.out.println("Previous Date: "+previousDate);
						readFromObsURL(previousDate, dateFormat.format(date));
						br.close();
					}else{
						readFromObsURL("1999-01-01 09:00:00-06",dateFormat.format(date));
						
						String updateValue = observationsList.get(observationsList.size()-1).getValid();
						PrintWriter out = new PrintWriter("update.txt");
						out.println(updateValue);
						out.close();
					}
								
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				JSONObject unitTemp = new JSONObject();
				JSONObject unitRelh = new JSONObject();
				JSONObject observedPropertyTemp= new JSONObject();
				JSONObject observedPropertyRelh= new JSONObject();
				
				for(Thing key : thingList){
					//System.out.println(key.getDescription());
					try {
						unitTemp.put("name", "Temperature");
						unitTemp.put("symbol", "�C");
						unitTemp.put("definition", "http://dbpedia.org/page/Celsius");
						
						unitRelh.put("name", "Relative Humidity");
						unitRelh.put("description", "Relative Humidity in %");
						unitRelh.put("symbol", "%");
						unitRelh.put("definition", "http://dbpedia.org/page/Relative_humidity");
						
						JSONObject jsonSensor = new JSONObject();
						jsonSensor.put("encodingType", "http://schema.org/description");
						jsonSensor.put("description", key.getDescription());
						jsonSensor.put("metadata", key.getBegInts()+","+key.getIem_network());
						
						observedPropertyTemp.put("name", "Temperature");
						observedPropertyTemp.put("description", "Air Temperature in Celsius, typically @ 2 meters");
						observedPropertyTemp.put("definition", "http://dbpedia.org/page/Temperature");
						
						observedPropertyRelh.put("name", "Relative Humidity");
						observedPropertyRelh.put("description", "Relative Humidity in %");
						observedPropertyRelh.put("definition", "http://dbpedia.org/page/Relative_humidity");
						
						addThingToDatabase(key,unitTemp,observedPropertyTemp,unitRelh ,observedPropertyRelh,jsonSensor);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				for(String key: hashMapThing.keySet()){
					getDatastreamID(key, hashMapThing.get(key));
				}
				
				try {
					PrintWriter printDatastreamID= new PrintWriter("datastream.txt");
					for(Integer key: hashmapDatastream.keySet()){
						printDatastreamID.println(key.intValue());
					}
					printDatastreamID.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				int dsID = 0;
				for(Observations n: observationsList){
					String[] tokenTime = n.getValid().split(" ");
					String newTime=tokenTime[0]+"T"+tokenTime[1]+":00.000+09:00";
					
					for(Integer key: hashmapDatastream.keySet()){
						if(hashmapDatastream.get(key).getStation().equals(n.getStation()) && hashmapDatastream.get(key).getUnit().equals("Temperature")){
							dsID=key.intValue();
							break;
						}
					}
					if(dsID != 0){
						addObservationsToDB(n.getTempf(),newTime,dsID);
					}
					
					for(Integer key: hashmapDatastream.keySet()){
						if(hashmapDatastream.get(key).getStation().equals(n.getStation()) && hashmapDatastream.get(key).getUnit().equals("Relative Humidity")){
							dsID=key.intValue();
							break;
						}
					}
					if(dsID != 0){
						addObservationsToDB(n.getRelh(),newTime,dsID);
					}
				}

			}
						
		};
		
		//run the program regularly everyday.
		final ScheduledExecutorService scheduler=Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(run, 1, 1, TimeUnit.DAYS);
		
	}
	
}