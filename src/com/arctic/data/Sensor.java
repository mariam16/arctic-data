package com.arctic.data;

public class Sensor{
	private String station;
	private String description;
	private String begInts;
	private String iem_network;
	
	public Sensor( String station, String description, String begInts,String iem_network){
		this.description=description;
		this.begInts = begInts;
		this.iem_network= iem_network;
		this.station=station;
	}
	
	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getBegInts() {
		return begInts;
	}

	public void setBegInts(String begInts) {
		this.begInts = begInts;
	}

	public String getIem_network() {
		return iem_network;
	}

	public void setIem_network(String iem_network) {
		this.iem_network = iem_network;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}